Kickit
======

Minimal config management. Minimal means:

  - Agent less, only ssh and bash
  - Single node configuration
  - Config format is simple and de-normalized and static

Progress
--------

Pretty much none.

Support
-------

  - Ubuntu 16.04+


CLI
---

### Flags

    -help
       print this short help screen
    -host string
       host you want to connect to and configure
    -host-key string
       Host key in authorized key format
    -private-key string
       path to the private key with which to connect (default "~/.ssh/key.pem")
    -user string
       user with which to connect to the remote machine (default "root")

### Declaration docu

(it's auto-generated from the validator and is still kind of ugly)

    .                      // Configuration declaration, object
    authorized-keys[]      // Authorized keys for ssh, list of object
    authorized-keys[].key  // key that is added to ~.ssh/authorized keys, string
    authorized-keys[].user // user for which key is added, string
    commands[]             // Commands to execute, list of object
    commands[].lines[]     // one line in the script, list of string
    commands[].user        // user that runs the commands, string
    files[]                // files that must be created on node, list of object
    files[].content        // content of the file to create, must be a string, optional string
    files[].file           // file from where to copy the content, optional string
    files[].folder         // folder from where to copy the files, optional string
    files[].group          // group sets the group the file belongs to, optional string
    files[].mod            // mod sets mode of file, for example 744, optional string
    files[].path           // path is the target file or folder, string
    files[].user           // user sets the owner of the file, string
    package-repos[]        // Package repos to add to package manager (apt), list of object
    package-repos[].key    // key http url, optional string
    package-repos[].line   // line in apt sources, optional string
    package-repos[].name   // name of the packet repo, optional string
    package-repos[].ppa    // ppa name, optional string
    packages[]             // Packages to install using package manager (apt), list of object
    packages[].debconf[]   // Debconf presets, list of string
    packages[].name        // name of the package, optional string
