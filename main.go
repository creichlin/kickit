package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations"
	"gitlab.com/creichlin/kickit/remote/ssh"
)

var (
	user       stringSlice
	host       stringSlice
	privateKey stringSlice
	hostKey    stringSlice
	help       bool
)

func main() {
	flag.BoolVar(&help, "help", false,
		"print this short help screen")

	flag.Var(&user, "user",
		"list of users with which to connect to the remote host")

	flag.Var(&host, "host",
		"list of hosts you want to connect to, last is the target, the others are used as jumphosts")

	flag.Var(&privateKey, "private-key",
		"list of paths to the private keys with which to connect")

	flag.Var(&hostKey, "host-key",
		"list of host key fingerprints, 'SHA256:base64encoded...'")

	flag.Parse()

	if help {
		flag.PrintDefaults()
		fmt.Println()
		fmt.Println(declaration.Help())
		return
	}

	if len(host) != len(user) || len(host) != len(privateKey) || len(host) != len(hostKey) {
		log.Fatalf("host, user, private-key and host-key must all have the same number of flags")
	}

	servers := []ssh.Server{}
	for i := range host {
		servers = append(servers, ssh.Server{
			Host:        host[i],
			User:        user[i],
			Key:         privateKey[i],
			Fingerprint: hostKey[i],
		})
	}

	connection, err := ssh.NewConnection(servers...)
	if err != nil {
		log.Fatal(err)
	}
	defer connection.Close()

	bytes, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		log.Fatal(err)
	}

	decl, err := declaration.ReadDeclaration(bytes)
	if err != nil {
		log.Fatal(err)
	}

	operations, err := operations.Collect(decl, true)
	if err != nil {
		log.Fatal(err)
	}

	err = execute.Apply(connection, operations)
	fmt.Println(connection.GetLog())
	if err != nil {
		log.Fatal(err)
	}
}

type stringSlice []string

func (i *stringSlice) String() string {
	return strings.Join(*i, ", ")
}

func (i *stringSlice) Set(value string) error {
	*i = append(*i, strings.TrimSpace(value))
	return nil
}
