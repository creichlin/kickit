module gitlab.com/creichlin/kickit

go 1.17

require (
	github.com/creichlin/goschema v0.0.0-20170919205605-c2a1e61f6185
	github.com/creichlin/gutil v0.0.0-20170720202424-7e90bad1fd27
	github.com/mitchellh/mapstructure v0.0.0-20170523030023-d0303fe80992
	golang.org/x/crypto v0.0.0-20170912191825-faadfbdc0353
	gopkg.in/yaml.v2 v2.0.0-20170812160011-eb3733d160e7
)

require (
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/xeipuuv/gojsonpointer v0.0.0-20170225233418-6fe8760cad35 // indirect
	github.com/xeipuuv/gojsonreference v0.0.0-20150808065054-e02fc20de94c // indirect
	github.com/xeipuuv/gojsonschema v0.0.0-20170528113821-0c8571ac0ce1 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
