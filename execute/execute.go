package execute

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/creichlin/kickit/remote"
)

const delimiter = "kickit-delimiter-aJopn97Q8hQAk"

func Apply(con remote.Connection, operations []Operation) error {
	script, analyzers := GenerateAnalyzerScript(operations)
	stdout, _, err := con.Run(script, nil)

	if err != nil {
		return err
	}

	executors, err := ParseResults(stdout, analyzers)
	if err != nil {
		return fmt.Errorf("Could not parse analyzer results, %v", err)
	}

	script, err = GenerateExecute(executors, con)
	if err != nil {
		return fmt.Errorf("Could generate execution script, %v", err)
	}

	_, _, err = con.Run(script, nil)
	if err != nil {
		return fmt.Errorf("Could not execute execution script, %v", err)
	}
	return nil
}

func GenerateExecute(executors []Executor, con remote.Connection) (*remote.Script, error) {
	scripts := remote.NewScript("")
	for _, x := range executors {
		script, err := x.Execute(con)
		if err != nil {
			return nil, err
		}
		scripts.AddScript(script)
	}
	return scripts, nil
}

func GenerateAnalyzerScript(ops []Operation) (*remote.Script, []Analyzer) {
	analyzers := []Analyzer{}
	analyzerScript := remote.NewScript("")
	for i, operation := range ops {
		script, analyzer := operation.Analyze()
		analyzerScript.Add("echo \"" + delimiter + " " + strconv.Itoa(i) + "\"")
		analyzerScript.AddScript(script)
		analyzers = append(analyzers, analyzer)
	}
	return analyzerScript, analyzers
}

func ParseResults(stdout string, analyzers []Analyzer) ([]Executor, error) {
	executors := []Executor{}
	currentLines := []string{}
	currentIndex := -1

	processItem := func() error {
		if currentIndex > -1 {
			executor, err := analyzers[currentIndex].Parse(currentLines)
			if err != nil {
				return err
			}
			executors = append(executors, executor)
		}
		return nil
	}

	for _, line := range strings.Split(stdout, "\n") {
		if strings.HasPrefix(line, delimiter) {
			err := processItem()
			if err != nil {
				return nil, err
			}
			currentLines = []string{}
			currentIndex, err = strconv.Atoi(line[len(delimiter)+1:])
			if err != nil {
				return nil, err
			}
		} else {
			currentLines = append(currentLines, line)
		}
	}
	// don't forget the last one
	err := processItem()
	if err != nil {
		return nil, err
	}

	if len(executors) != len(analyzers) {
		return nil, fmt.Errorf("number of executors must match number of analyzers after result parse step")
	}

	return executors, nil
}
