package execute

import (
	"gitlab.com/creichlin/kickit/remote"
)

type Operation interface {
	Analyze() (*remote.Script, Analyzer)
}

type Analyzer interface {
	Parse([]string) (Executor, error)
}

type Executor interface {
	Execute(connection remote.Connection) (*remote.Script, error)
}
