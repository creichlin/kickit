package remote

import (
	"io"
)

// Connection is a remote client where one can run bash scripts
// mainly for ssh used but for test mocks or possibly container API
// other implementations can be built
type Connection interface {
	// Run executes the provided script on the remote instance.
	// stdin is the remote process' standard input, if stdin is nil
	// the standard input is empty
	Run(script *Script, stdin io.Reader) (string, string, error)
}
