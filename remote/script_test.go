package remote

import "testing"

type scriptTestCase struct {
	script   *Script
	expected string
}

var scriptTestCases = []scriptTestCase{
	{
		script: NewScript("").
			Add("echo \"hello\""),
		expected: `echo "hello"`,
	},
	{
		script: NewScript("").
			Add("echo \"foo\"").
			Add("echo \"hello\""),
		expected: `echo "foo"
echo "hello"`,
	},
	{
		script: NewScript("").
			Add("echo \"hello\"").
			AddScript(NewScript("").Add("echo \"foo\"")),
		expected: `echo "hello"
echo "foo"`,
	},
	{
		script: NewScript("root").
			Add("echo \"hello\""),
		expected: `sudo su root << 'HDroot'
echo "hello"
HDroot`,
	},
	{
		script: NewScript("alice").
			Add("echo \"hello\"").
			AddScript(NewScript("bob").Add("echo \"foo\"")),
		expected: `sudo su alice << 'HDalice'
echo "hello"
sudo su bob << 'HDbob'
echo "foo"
HDbob
HDalice`,
	},
}

func TestDefaultScript(t *testing.T) {
	TestHeredocTag = true
	for _, testCase := range scriptTestCases {
		t.Run("", func(t *testing.T) {
			if testCase.script.Source() != testCase.expected {
				t.Errorf("generated script doesnt match expected script:\ngenerated:\n%v\nexpected:\n%v",
					testCase.script.Source(), testCase.expected)
			}
		})
	}
}
