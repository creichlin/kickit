package remote

import (
	"fmt"
	"math/rand"
	"strings"
)

var letters = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
var TestHeredocTag = false

type ScriptProvider interface {
	Source() string
}

type Script struct {
	lines []ScriptProvider
	user  string
}

type ScriptLine struct {
	line string
}

func NewScript(user string) *Script {
	return &Script{
		user: user,
	}
}

func (s *Script) Add(line string, params ...interface{}) *Script {
	s.lines = append(s.lines, &ScriptLine{fmt.Sprintf(line, params...)})
	return s
}

func (s *Script) AddScript(script ScriptProvider) *Script {
	s.lines = append(s.lines, script)
	return s
}

func (s *Script) Source() string {
	if len(s.lines) == 0 {
		return ""
	}
	strs := []string{}
	for _, line := range s.lines {
		strs = append(strs, line.Source())
	}
	code := strings.Join(strs, "\n")
	if s.user != "" {
		tag := heredocTag(s.user)
		return "sudo su " + s.user + " << '" + tag + "'\n" +
			code +
			"\n" + tag
	}
	return code
}

func (s *ScriptLine) Source() string {
	return s.line
}

func heredocTag(user string) string {
	if TestHeredocTag {
		return "HD" + user
	}

	b := make([]rune, 10)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return "GD" + string(b)
}
