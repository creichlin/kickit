package ssh

import (
	"fmt"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

func makeHost(s Server) string {
	return s.Host + ":22"
}

func makeConfig(s Server) (*ssh.ClientConfig, error) {
	signer, err := interpreteKey(s.Key)
	if err != nil {
		return nil, err
	}

	return &ssh.ClientConfig{
		User:            s.User,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: hostKeyFingerprint(s.Fingerprint),
		Timeout:         time.Second * 10,
	}, nil
}

func interpreteKey(key string) (ssh.Signer, error) {
	if isCertificate(key) {
		return fromSource(key)
	}
	return fromFile(key)
}

func fromSource(key string) (ssh.Signer, error) {
	return ssh.ParsePrivateKey([]byte(key))
}

func fromFile(key string) (ssh.Signer, error) {
	d, err := os.ReadFile(key)
	if err != nil {
		return nil, fmt.Errorf("could not read key from file %v, %w", key, err)
	}
	return fromSource(string(d))
}

func isCertificate(key string) bool {
	return strings.Contains(key, "-----BEGIN")
}
