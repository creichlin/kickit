package ssh

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net"
	"strings"

	"golang.org/x/crypto/ssh"
)

// hostKeyFingerprint returns a hostKeyCallback that checks the host key against the given
// fingerprint. Currently only `SHA256:DATA` fingerprints are allowed where data is base64
// encoded with trailing `=` chars removed.
// The SHA256: part is optional.
func hostKeyFingerprint(key string) ssh.HostKeyCallback {
	hk := &fingerprintHostKey{key}
	return hk.check
}

type fingerprintHostKey struct {
	fingerprint string
}

func (f *fingerprintHostKey) check(hostname string, remote net.Addr, key ssh.PublicKey) error {
	fp := strings.TrimPrefix(f.fingerprint, "SHA256:")
	hostFingerprint := calculateSHA256Fingerprint(key.Marshal())

	if hostFingerprint != fp {
		return fmt.Errorf("fingerprint mismatch %v != %v", hostFingerprint, fp)
	}

	return nil
}

func calculateSHA256Fingerprint(bytes []byte) string {
	hash := sha256.New()
	hash.Write(bytes) // Write never returns an error
	hashBytes := hash.Sum([]byte{})
	base64fingerprint := base64.StdEncoding.EncodeToString(hashBytes)
	return strings.TrimRight(base64fingerprint, "=")
}
