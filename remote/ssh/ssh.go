package ssh

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"net"

	"gitlab.com/creichlin/kickit/remote"
	"golang.org/x/crypto/ssh"
)

type Server struct {
	Host        string
	User        string
	Key         string
	Fingerprint string
}

// SSHConnection is an ssh remote. handles auth and collection of logs.
type SSHConnection struct {
	client   *ssh.Client
	clients  []*ssh.Client
	messages []message
}

type message struct {
	Type    string // Type of log: stdout,stderr,error,command
	Message string
}

// NewConnection creates a new SSHConnection to the last server parameter using the other
// servers as jump hosts.
func NewConnection(servers ...Server) (*SSHConnection, error) {
	if len(servers) == 0 {
		return nil, errors.New("must provide at least one server to connect to")
	}

	first := servers[0]

	config, err := makeConfig(first)
	if err != nil {
		return nil, err
	}

	clients := []*ssh.Client{}

	client, err := ssh.Dial("tcp", makeHost(first), config)
	if err != nil {
		return nil, err
	}

	log.Printf("connected to %v", makeHost(first))
	clients = append(clients, client)

	for _, server := range servers[1:] {
		con, err := client.Dial("tcp", makeHost(server))
		if err != nil {
			return nil, err
		}

		config, err := makeConfig(server)
		if err != nil {
			return nil, err
		}

		client, err = dialConn(con, makeHost(server), config)
		if err != nil {
			return nil, err
		}

		log.Printf("connected to %v", makeHost(server))
		clients = append(clients, client)
	}

	return &SSHConnection{
		client:  client,
		clients: clients,
	}, nil
}

func dialConn(conn net.Conn, addr string, config *ssh.ClientConfig) (*ssh.Client, error) {
	c, chans, reqs, err := ssh.NewClientConn(conn, addr, config)
	if err != nil {
		return nil, err
	}
	return ssh.NewClient(c, chans, reqs), nil
}

// GetLog gets all logs of commands that got executed in this connection
func (c *SSHConnection) GetLog() string {
	out := ""

	for _, message := range c.messages {
		out += fmt.Sprintf("%v:\n%v\n", message.Type, message.Message)
	}

	return out
}

func (c *SSHConnection) log(kind string, msg string) {
	c.messages = append(c.messages, message{kind, msg})
}

func (c *SSHConnection) logRun(cmd, stdout, stderr string, err error) {
	c.log("command", cmd)

	if stdout != "" {
		c.log("stdout", stdout)
	}

	if err != nil {
		c.log("error", err.Error())
	}

	if stderr != "" {
		c.log("stderr", stderr)
	}
}

// Run executes the provided script on the remote instance.
// stdin is the remote process' standard input, if stdin is nil
// the standard input is empty.
// Each command as well as it's stdout and stderr will be added to the log.
// Stdin will not be logged as it usually contains data (file upload and such things)
func (c *SSHConnection) Run(script *remote.Script, reader io.Reader) (string, string, error) {
	session, err := c.client.NewSession()
	if err != nil {
		return "", "", err
	}
	defer session.Close()

	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf

	var stderrBuf bytes.Buffer
	session.Stderr = &stderrBuf

	session.Stdin = reader

	err = session.Run(script.Source())

	result := stdoutBuf.String()
	stderr := stderrBuf.String()

	c.logRun(script.Source(), result, stderr, err)

	return result, stderr, err
}

// Closes the connection
func (c *SSHConnection) Close() {
	c.client.Close()
}
