package declaration

import (
	"github.com/creichlin/goschema"
	"github.com/creichlin/gutil/treedata"
	"github.com/mitchellh/mapstructure"
	"gopkg.in/yaml.v2"
)

type Declaration struct {
	Files          []*File
	AuthorizedKeys []*AuthorizedKey `mapstructure:"authorized-keys"`
	Packages       []*Package
	PackageRepos   []*PackageRepo `mapstructure:"package-repos"`
	Commands       []*Commands
	Services       []*Service
}

type Commands struct {
	User  string
	Lines []string
}

type Package struct {
	Name    string
	Debconf []string
}

type PackageRepo struct {
	PPA  string `mapstructure:"ppa"`
	Line string
	Key  string
	Name string
}

type AuthorizedKey struct {
	Key  string
	User string
}

type File struct {
	Path    string
	User    string
	Group   string
	Content string
	Folder  string
	File    string
	Mod     string
}

type Service struct {
	Name     string
	ExecPath string `mapstructure:"exec-path"`
	Env      map[string]string
}

// Help prints the declaration syntax used for validating the declaration
func Help() string {
	schema := buildConfigSchema()
	return goschema.Doc(schema)
}

// ReadDeclaration will read the yaml input, validate it
// and build the declaration structure
func ReadDeclaration(declaration []byte) (*Declaration, error) {
	data, err := readDataFrom([]byte(declaration))
	if err != nil {
		return nil, err
	}
	return ReadDeclarationFromTree(data)
}

func ReadDeclarationFromTree(data interface{}) (*Declaration, error) {
	data = treedata.SanitizeForJSON(data)

	schema := buildConfigSchema()
	errors := goschema.ValidateGO(schema, data)
	if errors.Has() {
		return nil, errors
	}

	config := &Declaration{}
	err := mapstructure.Decode(data, config)
	errors.Add(err)
	if errors.Has() {
		return nil, errors
	}
	return config, nil
}

func readDataFrom(binData []byte) (interface{}, error) {
	data := interface{}(nil)

	err := yaml.Unmarshal(binData, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func buildConfigSchema() goschema.Type {
	return goschema.NewObjectType("Configuration declaration", func(o goschema.ObjectType) {
		o.Optional("files").List(func(m goschema.ListType) {
			m.Object("files that must be created on node", func(o goschema.ObjectType) {
				o.Attribute("path").String("is the target file or folder")
				o.Attribute("user").String("sets the owner of the file")
				o.Optional("group").String("sets the group the file belongs to")
				o.Optional("content").String("of the file to create, must be a string")
				o.Optional("folder").String("from where to copy the files")
				o.Optional("file").String("from where to copy the content")
				o.Optional("mod").String("sets mode of file, for example 744")
			})
		})
		o.Optional("authorized-keys").List(func(l goschema.ListType) {
			l.Object("Authorized keys for ssh", func(o goschema.ObjectType) {
				o.Attribute("key").String("that is added to ~.ssh/authorized keys")
				o.Attribute("user").String("for which key is added")
			})
		})
		o.Optional("packages").List(func(l goschema.ListType) {
			l.Object("Packages to install using package manager (apt)", func(o goschema.ObjectType) {
				o.Optional("name").String("of the package")
				o.Optional("debconf").List(func(l goschema.ListType) {
					l.String("Debconf presets")
				})
			})
		})
		o.Optional("package-repos").List(func(l goschema.ListType) {
			l.Object("Package repos to add to package manager (apt)", func(o goschema.ObjectType) {
				o.Optional("ppa").String("name")
				o.Optional("name").String("of the packet repo")
				o.Optional("key").String("http url")
				o.Optional("line").String("in apt sources")
			})
		})
		o.Optional("commands").List(func(l goschema.ListType) {
			l.Object("Commands to execute", func(o goschema.ObjectType) {
				o.Attribute("user").String("that runs the commands")
				o.Attribute("lines").List(func(l goschema.ListType) {
					l.String("one line in the script")
				})
			})
		})
		o.Optional("services").List(func(l goschema.ListType) {
			l.Object("Services to install", func(o goschema.ObjectType) {
				o.Attribute("name").String("name of the service")
				o.Attribute("exec-path").String("Path to the executable")
				o.Attribute("env").Map(func(m goschema.MapType) {
					m.String("env var value")
				})
			})
		})
	})
}
