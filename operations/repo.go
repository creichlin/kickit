package operations

import (
	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func PacketRepo(repo *declaration.PackageRepo) (execute.Operation, error) {
	if repo.PPA != "" {
		return atom.NewPPAPacketRepoOperation(repo.PPA), nil
	}

	return atom.NewPacketRepoOperation(repo.Name, repo.Line, repo.Key), nil
}
