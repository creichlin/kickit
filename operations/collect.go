package operations

import (
	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
)

func Collect(config *declaration.Declaration, aptUpdate bool) ([]execute.Operation, error) {
	allOperations := []execute.Operation{}

	for _, key := range config.AuthorizedKeys {
		operation := AuthorizedKey(key)
		allOperations = append(allOperations, operation)
	}

	for _, file := range config.Files {
		ops, err := File(file)
		if err != nil {
			return nil, err
		}

		allOperations = append(allOperations, ops...)
	}

	for _, repo := range config.PackageRepos {
		operation, err := PacketRepo(repo)
		if err != nil {
			return nil, err
		}

		allOperations = append(allOperations, operation)
	}

	if aptUpdate {
		operation := RepoUpdate()
		allOperations = append(allOperations, operation)
	}

	for _, pkg := range config.Packages {
		operation := Package(pkg)
		allOperations = append(allOperations, operation)
	}

	for _, srvc := range config.Services {
		operation := Service(srvc)
		allOperations = append(allOperations, operation...)
	}

	for _, commands := range config.Commands {
		operation := Commands(commands)
		allOperations = append(allOperations, operation)
	}

	return allOperations, nil
}
