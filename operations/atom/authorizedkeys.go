package atom

import (
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

type AuthorizedKeyOperation struct {
	key  string
	user string
}

type AuthorizedKeyAnalyzer struct {
	operation *AuthorizedKeyOperation
}

type AuthorizedKeyExecutor struct {
	operation *AuthorizedKeyOperation
}

func (a *AuthorizedKeyOperation) Analyze() (*remote.Script, execute.Analyzer) {
	return remote.NewScript("").
			Add("echo \"key\""),
		&AuthorizedKeyAnalyzer{
			operation: a,
		}
}

func (a *AuthorizedKeyAnalyzer) Parse(in []string) (execute.Executor, error) {
	return &AuthorizedKeyExecutor{
		operation: a.operation,
	}, nil
}

func (a *AuthorizedKeyExecutor) Execute(c remote.Connection) (*remote.Script, error) {
	return remote.NewScript(a.operation.user).
		Add("if [ ! -f ~/.ssh/authorized_keys ]; then").
		Add("  touch ~/.ssh/authorized_keys").
		Add("  chmod 600 ~/.ssh/authorized_keys").
		Add("fi").
		Add("if grep -Fxq \"" + a.operation.key + "\" ~/.ssh/authorized_keys; then").
		Add("  echo \"already there\"").
		Add("else").
		Add("  echo \"" + a.operation.key + "\" >> ~/.ssh/authorized_keys").
		Add("fi"), nil
}

func NewAuthorizedKey(key, user string) execute.Operation {
	return &AuthorizedKeyOperation{
		key:  key,
		user: user,
	}
}
