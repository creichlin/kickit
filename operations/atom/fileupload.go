package atom

import (
	"bytes"
	"crypto/md5" // nolint gosec G401
	"fmt"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

type fileUploadOperation struct {
	path        string
	contentData []byte
	expected    state
}

type fileUploadAnalyzer struct {
	operation *fileUploadOperation
}

type fileUploadExecutor struct {
	operation *fileUploadOperation
	current   state
}

type state struct {
	user  string
	group string
	mod   string
	md5   string
}

func NewFileUpload(path, user, group, mod string, content []byte) execute.Operation {
	return &fileUploadOperation{
		path: path,
		expected: state{
			user:  user,
			group: group,
			mod:   mod,
			md5:   fmt.Sprintf("%x", md5.Sum(content)), // nolint gosec G401
		},
		contentData: content,
	}
}

// Analyze returns a script that produces the files hash and the files mod/user/group
func (f *fileUploadOperation) Analyze() (*remote.Script, execute.Analyzer) {
	script := remote.NewScript("root").
		Add(`if [ -f "%v" ] ; then md5sum "%v" | cut -c-32 ; else echo "00000000000000000000000000000000" ; fi`,
			f.path, f.path).
		Add(`if [ -f "%v" ] ; then stat -c "%%a,%%U,%%G" "%v" ; else echo "000,undef,undef" ; fi`,
			f.path, f.path)

	return script, &fileUploadAnalyzer{
		operation: f,
	}
}

// Parse parses the result from the analyze script and creates the executer
func (f *fileUploadAnalyzer) Parse(lines []string) (execute.Executor, error) {
	if len(lines) < 2 || len(lines[0]) != 32 {
		return nil, fmt.Errorf("Failed to calculate remote md5 and permissions from %v", f.operation.path)
	}

	currentState := state{}
	currentState.md5 = lines[0]

	perms := strings.Split(lines[1], ",")
	if len(perms) != 3 {
		return nil, fmt.Errorf("received wrong perms for %v, %v", f.operation.path, lines[1])
	}

	currentState.mod = perms[0]
	currentState.user = perms[1]
	currentState.group = perms[2]

	return &fileUploadExecutor{
		operation: f.operation,
		current:   currentState,
	}, nil
}

// Execute will create the file and sets permissions if necessary
// uploading the file-content will be done directly over connection,
// setting permissions will be returned as a script for cumulative
// update
func (f *fileUploadExecutor) Execute(con remote.Connection) (*remote.Script, error) {
	expected := f.operation.expected

	if f.current.md5 != expected.md5 {
		scr := remote.NewScript(expected.user).Add(`mkdir -p "%v"`, path.Dir(f.operation.path))
		_, _, err := con.Run(scr, nil)

		if err != nil {
			return nil, err
		}

		// make a temp file name for target host
		tmpFile := filepath.Join(filepath.Dir(f.operation.path), "."+filepath.Base(f.operation.path)+".unkick")

		// the script will send file contents over stdin trough tee to temp file and will rename it
		// to proper filename after
		script := remote.NewScript("").
			Add(`sudo tee "%v" > /dev/null ; sudo mv "%v" "%v"`,
				tmpFile, tmpFile, f.operation.path)

		_, _, err = con.Run(script, bytes.NewReader(f.operation.contentData))
		if err != nil {
			return nil, err
		}

		// after creating a file we have to set permissions anyways
		return f.executePermissions(
			state{user: "undef", group: "undef", mod: "000"},
			expected, f.operation.path,
		), nil
	}

	return f.executePermissions(f.current, expected, f.operation.path), nil
}

func (f *fileUploadExecutor) executePermissions(current, expected state, path string) *remote.Script {
	script := remote.NewScript("root")
	if expected.user != "" && expected.user != current.user {
		script.Add(`chown %v "%v"`, expected.user, path)
	}

	if expected.group != "" && expected.group != current.group {
		script.Add(`chgrp %v "%v"`, expected.group, path)
	}

	if expected.mod != "" && expected.mod != current.mod {
		script.Add(`chmod %v "%v"`, expected.mod, path)
	}

	return script
}
