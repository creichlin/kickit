package atom

import (
	"fmt"
	"strings"

	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

func NewPackage(name string, debconf []string) execute.Operation {
	return &packageOperation{
		name:    name,
		debconf: debconf,
	}
}

type packageOperation struct {
	name    string
	debconf []string
}

type packageAnalyzer struct {
	operation *packageOperation
}

type packageExecutor struct {
	operation *packageOperation
	installed bool
}

func (a *packageOperation) Analyze() (*remote.Script, execute.Analyzer) {
	return remote.NewScript("").
			Add("dpkg -s " + a.name + " 2>&1 | egrep '^(Version|Status):' || " +
				"printf \"Status: not found\\nVersion: unknown\\n\""),
		&packageAnalyzer{
			operation: a,
		}
}

func (a *packageAnalyzer) Parse(in []string) (execute.Executor, error) {
	in = stripEmpty(in)
	if len(in) != 2 {
		return nil, fmt.Errorf("Failed to parse package analyzer, %v", strings.Join(in, "\n"))
	}

	return &packageExecutor{
		operation: a.operation,
		installed: strings.HasSuffix(in[0], "installed"),
	}, nil
}

func (a *packageExecutor) Execute(c remote.Connection) (*remote.Script, error) {
	if a.installed {
		return remote.NewScript(""), nil
	}

	script := remote.NewScript("root")
	for _, dc := range a.operation.debconf {
		script.Add("echo \"debconf %v\" | debconf-set-selections", dc)
	}

	script.Add("export DEBIAN_FRONTEND=noninteractive")
	script.Add("apt-get install -y %v", a.operation.name)

	return script, nil
}

func stripEmpty(i []string) []string {
	r := []string{}

	for _, l := range i {
		if len(strings.Trim(l, " ")) != 0 {
			r = append(r, l)
		}
	}

	return r
}
