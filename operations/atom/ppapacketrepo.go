package atom

import (
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

func NewPPAPacketRepoOperation(ppa string) execute.Operation {
	return &ppaPacketRepoOperation{ppa: ppa}
}

type ppaPacketRepoOperation struct {
	ppa string
}

type ppaPacketRepoAnalyzer struct {
	operation *ppaPacketRepoOperation
}

type ppaPacketRepoExecutor struct {
	operation *ppaPacketRepoOperation
}

func (a *ppaPacketRepoOperation) Analyze() (*remote.Script, execute.Analyzer) {
	return remote.NewScript("").
			Add("echo \"packet repo\""),
		&ppaPacketRepoAnalyzer{
			operation: a,
		}
}

func (a *ppaPacketRepoAnalyzer) Parse(in []string) (execute.Executor, error) {
	return &ppaPacketRepoExecutor{
		operation: a.operation,
	}, nil
}

func (a *ppaPacketRepoExecutor) Execute(c remote.Connection) (*remote.Script, error) {
	script := remote.NewScript("root").
		Add("if ! grep -sq \"^deb .*/%v/\" /etc/apt/sources.list /etc/apt/sources.list.d/*.list", a.operation.ppa).
		Add("then").
		Add("add-apt-repository ppa:%v", a.operation.ppa).
		Add("fi")

	return script, nil
}
