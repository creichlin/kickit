package atom

import (
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

type commandsOperation struct {
	lines []string
	user  string
}

type commandsAnalyzer struct {
	operation *commandsOperation
}

type commandsExecutor struct {
	operation *commandsOperation
}

func (a *commandsOperation) Analyze() (*remote.Script, execute.Analyzer) {
	return remote.NewScript("").
			Add("echo \"commands\""),
		&commandsAnalyzer{
			operation: a,
		}
}

func (a *commandsAnalyzer) Parse(in []string) (execute.Executor, error) {
	return &commandsExecutor{
		operation: a.operation,
	}, nil
}

func (a *commandsExecutor) Execute(c remote.Connection) (*remote.Script, error) {
	script := remote.NewScript(a.operation.user)
	for _, line := range a.operation.lines {
		script.Add(line)
	}

	return script, nil
}

func NewCommands(commands []string, user string) execute.Operation {
	return &commandsOperation{
		lines: commands,
		user:  user,
	}
}
