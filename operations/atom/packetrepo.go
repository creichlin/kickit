package atom

import (
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/remote"
)

func NewPacketRepoOperation(name, line, key string) execute.Operation {
	return &packetRepoOperation{
		name: name,
		line: line,
		key:  key,
	}
}

type packetRepoOperation struct {
	name string
	line string
	key  string
}

type packetRepoAnalyzer struct {
	operation *packetRepoOperation
}

type packetRepoExecutor struct {
	operation *packetRepoOperation
}

func (a *packetRepoOperation) Analyze() (*remote.Script, execute.Analyzer) {
	return remote.NewScript("").
			Add("echo \"packet repo\""),
		&packetRepoAnalyzer{
			operation: a,
		}
}

func (a *packetRepoAnalyzer) Parse(in []string) (execute.Executor, error) {
	return &packetRepoExecutor{
		operation: a.operation,
	}, nil
}

func (a *packetRepoExecutor) Execute(c remote.Connection) (*remote.Script, error) {
	script := remote.NewScript("root").
		Add("echo \"%v\" > /etc/apt/sources.list.d/%v.list", a.operation.line, a.operation.name).
		Add("wget -qO - \"%v\" | apt-key add -", a.operation.key)

	return script, nil
}
