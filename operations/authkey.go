package operations

import (
	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func AuthorizedKey(key *declaration.AuthorizedKey) execute.Operation {
	return atom.NewAuthorizedKey(key.Key, key.User)
}
