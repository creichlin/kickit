package operations

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

// File constructs file operations from declaration
func File(file *declaration.File) ([]execute.Operation, error) {
	if file.Content != "" {
		return createFileFromContent(file), nil
	}

	if file.Folder != "" {
		return createFilesFromFolder(file)
	}

	if file.File != "" {
		return createFileFromFile(file)
	}

	return nil, fmt.Errorf("no source given for file")
}

func createFileFromFile(file *declaration.File) ([]execute.Operation, error) {
	content, err := ioutil.ReadFile(file.File)
	if err != nil {
		return nil, err
	}

	return []execute.Operation{
		atom.NewFileUpload(file.Path, file.User, file.Group, file.Mod, content),
	}, nil
}

func createFilesFromFolder(file *declaration.File) ([]execute.Operation, error) {
	ops := []execute.Operation{}
	err := filepath.Walk(file.Folder, func(filePath string, f os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if f.IsDir() {
			return nil
		}
		content, err := ioutil.ReadFile(filePath)
		if err != nil {
			return err
		}

		relative, err := filepath.Rel(file.Folder, filePath)
		if err != nil {
			return err
		}

		upload := atom.NewFileUpload(path.Join(file.Path, relative),
			file.User, file.Group, file.Mod, content)

		ops = append(ops, upload)
		return nil
	})

	return ops, err
}

func createFileFromContent(file *declaration.File) []execute.Operation {
	return []execute.Operation{
		atom.NewFileUpload(file.Path, file.User, file.Group, file.Mod, []byte(file.Content)),
	}
}
