package operations

import (
	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func Package(dec *declaration.Package) execute.Operation {
	return atom.NewPackage(dec.Name, dec.Debconf)
}
