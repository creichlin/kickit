package operations

import (
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func RepoUpdate() execute.Operation {
	return atom.NewCommands([]string{
		"export DEBIAN_FRONTEND=noninteractive",
		"apt-get update",
	}, "root")
}
