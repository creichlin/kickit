package operations

import (
	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func Commands(commands *declaration.Commands) execute.Operation {
	return atom.NewCommands(commands.Lines, commands.User)
}
