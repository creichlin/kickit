package operations

import (
	"fmt"

	"gitlab.com/creichlin/kickit/declaration"
	"gitlab.com/creichlin/kickit/execute"
	"gitlab.com/creichlin/kickit/operations/atom"
)

func Service(svc *declaration.Service) []execute.Operation {
	ops := []execute.Operation{}

	env := ""
	for nam, value := range svc.Env {
		env += fmt.Sprintf("Environment=%v=\"%v\"\n", nam, value)
	}

	srv := fmt.Sprintf(`
[Unit]
Description=%v service, managed by kickit
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=%v
Restart=always
%v
[Install]
WantedBy=multi-user.target
`, svc.Name, svc.ExecPath, env)

	ops = append(ops, atom.NewFileUpload(
		fmt.Sprintf("/etc/systemd/system/%v.service", svc.Name), "root", "root", "600", []byte(srv)))

	ops = append(ops, atom.NewCommands([]string{
		fmt.Sprintf("systemctl enable %v.service", svc.Name),
		fmt.Sprintf("systemctl start %v.service", svc.Name),
	}, "root"))

	return ops
}
